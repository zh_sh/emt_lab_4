package mk.ukim.finki.emt.ordermanagement.domain.valueobjects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import mk.ukim.finki.emt.sharedkernel.domain.base.ValueObject;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Currency;
import mk.ukim.finki.emt.sharedkernel.domain.financial.Money;

@Getter
public class Product implements ValueObject {

    private final ProductId id;
    private final String name;
    private final String description;
    private final String type;
    private final String manufacturerName;
    private final Money basePrice;
    private final Money currentSalesPriceCut;
    private final int availableUnits;
    private final int sales;

    private Product() {
        this.id=ProductId.randomId(ProductId.class);
        this.name= "";
        this.description= "";
        this.type= "";
        this.manufacturerName= "";
        this.basePrice = Money.valueOf(Currency.MKD,0);
        this.currentSalesPriceCut = Money.valueOf(Currency.MKD,0);
        this.availableUnits = 0;
        this.sales = 0;
    }

    @JsonCreator
    public Product(@JsonProperty("id") ProductId id,
                   @JsonProperty("productName") String name,
                   @JsonProperty("description") String description,
                   @JsonProperty("type") String type,
                   @JsonProperty("manufacturerName") String manufacturerName,
                   @JsonProperty("price") Money price,
                   @JsonProperty("salesPriceCut") Money salesPriceCut,
                   @JsonProperty("availableUnits") int availableUnits,
                   @JsonProperty("sales") int sales
                   ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.manufacturerName = manufacturerName;
        this.basePrice = price;
        this.currentSalesPriceCut = salesPriceCut;
        this.availableUnits = availableUnits;
        this.sales = sales;
    }
}
